// Practico de introduccion.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

void ejercicio2(int numerador, int denominador) {

	//Si el numerador o denominador es negativo entonces hago negado. 
	if (numerador < 0) {
		numerador = -numerador;
	}
	if (denominador < 0) {
		denominador = -denominador;
	}


	int numeradorSimplificado = numerador;
	int denominadorSimplificado = denominador;

	for (int i = 1; i <= numerador; i++)
	{
		if (numerador % i == 0 && denominador % i == 0) {
			numeradorSimplificado = numerador / i;
			denominadorSimplificado = denominador / i;
		}
	}

	cout << numeradorSimplificado << " / " << denominadorSimplificado;
}

void ejercicio3(int primerCateto, int segundoCateto, int hipotenusa) {
	if (primerCateto > 0 && segundoCateto > 0 && hipotenusa > 0) {
		if (((primerCateto * primerCateto) + (segundoCateto * segundoCateto)) == hipotenusa * hipotenusa) {
			cout << "Los numeros ingresados pueden ser longitudes de un triangulo.";
		}
		else
		{
			cout << "Los numeros ingresados NO pueden ser longitudes de un triangulo.";
		}
	}
	else {
		cout << "Debe ingresar numeros positivos.";
	}
}

void ejercicio5(int n) {

	int recorrido = n;
	int numeroIngresado = 0;
	int promedio = 0;
	int cantidadPares = 0;
	int multiplosDeTres = 0;
	while (recorrido > 0) {
		cout << "Ingrese un numero: ";
		cin >> numeroIngresado;
		if (numeroIngresado % 2 == 0) {
			cantidadPares++;
		}
		if (numeroIngresado % 3 == 0) {
			multiplosDeTres++;
		}
		promedio += numeroIngresado;
		recorrido--;
	}
	promedio = promedio / n;
	cout << "El promedio es: " << promedio << endl << cantidadPares << " fueron pares." << endl << multiplosDeTres << " fueron multiplos de tres.";


}

void ejercicio5b() {
	int recorrido = 0;
	int numeroIngresado = 1;
	int promedio = 0;
	int cantidadPares = 0;
	int multiplosDeTres = 0;
	while (numeroIngresado != 0) {
		cout << "Ingrese un numero: ";
		cin >> numeroIngresado;
		if (numeroIngresado != 0) {
			if (numeroIngresado % 2 == 0) {
				cantidadPares++;
			}
			if (numeroIngresado % 3 == 0) {
				multiplosDeTres++;
			}
			promedio += numeroIngresado;
			recorrido++;
		}
	}
	promedio = promedio / recorrido;
	cout << "El promedio es: " << promedio << endl << cantidadPares << " fueron pares." << endl << multiplosDeTres << " fueron multiplos de tres.";
}

void ejercicio6(int n) {

	int numeroAnterior = INT16_MIN;
	int numeroActual;
	int cantidadSecuencias = 0;
	bool haySecuencia = false;
	while (n >= 0) {
		if (n > 0)
		{
			cout << "Ingrese un numero: ";
			cin >> numeroActual;
		}
		if (numeroActual > numeroAnterior) {
			haySecuencia = true;
		}
		else if (numeroActual <= numeroAnterior && haySecuencia == true) {
			cantidadSecuencias++;
			haySecuencia = false;
		}

		numeroAnterior = numeroActual;
		n--;
	}
	cout << "Hubo " << cantidadSecuencias << " secuencias.";
}

void ejercicio7() {
	int numero = INT16_MIN;
	int valorDeterminado;
	float porcentaje = 0;
	int promedioSalarioGeneral = 0;
	int promedioSalarioDeterminado = 0;
	float contadorGeneral = 0;
	float contadorDeterminado = 0;
	cout << "Ingrese un valor inicial para comparar: ";
	cin >> valorDeterminado;
	while (numero != 0) {

		cout << "Ingrese un numero mayor a cero que desee calcular o '0' para terminar: ";
		cin >> numero;
		if (numero != 0) {
			promedioSalarioGeneral += numero;

			if (numero >= valorDeterminado) {
				promedioSalarioDeterminado += numero;
				contadorDeterminado++;
			}
			contadorGeneral++;
		}


	}
	porcentaje = (contadorDeterminado / contadorGeneral) * 100;
	cout << "Porcentaje > a " << valorDeterminado << " = " << porcentaje << "%." << endl;
	cout << "Promedio de sueldos > a " << valorDeterminado << " = $" << (promedioSalarioDeterminado / contadorDeterminado) << endl;
	cout << "Promedio general de sueldos de la empresa = " << (promedioSalarioGeneral / contadorGeneral);
}

void ejercicio8() {

	int maximo = INT16_MIN;
	int sumaHastaMaximo = 0;
	int restaHastaMaximo = 0;
	int elementoLista;
	int largo;
	cout << "Ingrese el largo de la lista: ";
	cin >> largo;

	while (largo != 0) {
		cout << "Ingrese un elemento de la lista: ";
		cin >> elementoLista;
		restaHastaMaximo += elementoLista;
		if (elementoLista > maximo) {
			maximo = elementoLista;
			restaHastaMaximo = 0;
		}
		sumaHastaMaximo += elementoLista;
		largo--;
	}
	cout << "Maximo: " << maximo << endl;
	cout << "Suma hasta el maximo: " << sumaHastaMaximo - restaHastaMaximo;

}

void ejercicio9() {
	bool haySecuencia = false;
	int numero = INT16_MIN;
	int numeroAnterior = 0;
	int contador = 0;

	while (numero != 0) {
		cout << "Ingrese un numero (1, 2 o 3) o 0 para terminar: ";
		cin >> numero;
		if (numero <= 3 && numero >= 0) {
			if (numero == 1) {
				haySecuencia = true;
				numeroAnterior = numero;
			}
			else if (numero == 2 && numeroAnterior == 1) {
				numeroAnterior = numero;
			}
			else if (numero == 3 && numeroAnterior == 2 && haySecuencia == true) {
				contador++;
				haySecuencia = false;
			}
			else
			{
				haySecuencia = false;
			}
		}
		else {
			cout << "El numero ingresado debe ser 0, 1, 2 o 3.";
		}
	}
	cout << "Hay " << contador << " subsecuencias" << endl;

}

void ejercicio9b() {
	bool haySecuencia = false;
	int numero = INT16_MIN;
	int numeroAnterior = 0;
	int contador = 0;

	while (numero != 0) {
		cout << "Ingrese un numero (1, 2 o 3) o 0 para terminar: ";
		cin >> numero;
		if (numero <= 3 && numero >= 0) {
			if (numero == 1) {
				haySecuencia = true;
				numeroAnterior = numero;
			}
			else if (numero == 2 && ((numeroAnterior == 1) || (numeroAnterior == 2 && haySecuencia == true))) {
				numeroAnterior = numero;
			}
			else if (numero == 3 && numeroAnterior == 2 && haySecuencia == true) {
				contador++;
				haySecuencia = false;
			}
			else
			{
				haySecuencia = false;
			}
		}
		else {
			cout << "El numero ingresado debe ser 0, 1, 2 o 3.";
		}
	}
	cout << "Hay " << contador << " subsecuencias" << endl;
}

void ejercicio10(int* a, int* b) {
	int* aux = a;
	a = b;
	b = aux;
}

//Ejercicio11
int misterio(char* p) {
	char* t = p;
	while (*t != '\0') {
		t++;
	}
	return t � p;
}
// Recorre la palabra 'p' pasada por parametro 


//Ejercicio 12
void misterio(char* p, char* t) {
	while (*p++ = *t++);
}

//Ejercicio 13
int misterio(char* s, char* t) {
	for (; *s == *t; s++, t++)
		if (*s == '\0')
			return 0;
	return *s - *t;
}
//Recorre la palabra 'S' y la palabra 't' pasadas por parametro. 


int main()
{
	int a, b;
	cout << "Ingresa un numero: " << endl;
	cin >> a;
	cout << "Ingresa otro numero: " << endl;
	cin >> b;
	/*cout << "Ingrese una hipotenusa: ";
	cin >> c;*/
	//ejercicio10(a, b);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

