// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

int main()
{
    /*Los punteros son valores que representan direcciones de memoria.
    Pensando en un variable int sencilla:
    */
    int x = 42;
    /* 42 es su valor. No sabemos donde esta guardado ese '42', pero sabemos que 
    * esta guardado en algun casillero de la memoria y que ese casillero tiene un numero.
    * podemos averiguar donde esta guardado usando '&'
    */
    cout << "El valor '42' esta guardado en la direccion>: " << &x << endl;
    /* Sabiendo la direccion, que solo es un numero, podemos guardarala como valor en una variable.
    * Su tipo no va a ser int, sino que va a ser el "el tipo de las direcciones a int".
    */
    int* a = &x;
    cout << "El valor de 'A' es: " << a << endl;
    /* Puedo transformar una direccion en su valor. Es decir, pasar el valor 'A' que es la direccion, al valor de 'X' que es a donde esta apuntando
    * Para ello utilizo el '*' antes de la variable.
    */
    cout << "El valor de lo que apuntado por 'A' es: " << *a << endl;
    /*Solo 'A' es el valor de 'A'
    * '&A' es donde esta 'A'
    * '*A' es a donde va 'A'
    */
    cout << "La direccion donde esta 'A' es: " << &a << endl;
    cout << "Si voy a la direccion donde esta 'A': " << *(&a) << endl;
    cout << "Si voy a la direccion donde esta 'A', para luego usar ese valor como direccion: " << *(*(&a)) << endl;

    int y = x; // Se copia el '42' a 'Y'. Ahora tengo dos '42' independientes.
    int* b = a; // Se copia la direccion que esta en 'A' hacia 'B'
    //Tenemos dos copias de la direccion.
    *a++; //Es lo mismo que decir 'X++'
    cout << "X: " << x << " Y: " << y << " *A:" << *a << " *B: " << b << endl;
    //Nota: solo modificamos 'X' pero '*A = X', '*B = X'.

    /* Por comodidad tambien existe el tipo '&'
    * este es en verdad un puntero, pero que por comodidad tiene implicitos los asteriscos.
    */
    int& c = x;
    // Es parecido a decir 'int* c = &x;' pero siempre que los usamos tiene un '*' implicito.
    cout << "C: " << c << endl;
    c++;
    cout << "X: " << x << " Y: " << y << " *A: " << *a << " *B: " << *b << " C: " << c << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
