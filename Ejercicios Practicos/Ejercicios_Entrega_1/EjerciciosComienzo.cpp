#include "EjerciciosComienzo.h"

int suma(int a, int b) {
	return a + b;
}

void tablaDel(unsigned int tablaDel, unsigned int desde, unsigned int hasta) {
	int valor = desde;
	while (valor <= hasta) {
		cout << valor << "*" << tablaDel << "=" << (valor * tablaDel);
		if (valor < hasta) {
			cout << ";";
		}
		valor++;
	}
}

void simplificar(int n, int d) {
	// IMPLEMENTAR SOLUCION
	int numerador = 1;
	int denominador = 1;
	int recorrido;
	bool negativo = false;
	if (n < 0) {
		n = n * -1;
		negativo = true;
	}
	if (d < 0) {
		d = d * -1;
		if (negativo == true) {
			negativo = false;
		}
		else {
			negativo = true;
		}
	}
	if (numerador >= denominador) {
		recorrido = n;
	}
	else
	{
		recorrido = d;
	}

	for (int i = 1; i <= recorrido; i++) {
		if (n % i == 0 && d % i == 0) {
			numerador = n / i;
			denominador = d / i;
		}
	}
	if (negativo) {
		cout << "-" << numerador << "/" << denominador;
	}
	else {
		cout << numerador << "/" << denominador;
	}
}

int ocurrencias123Repetidos(int* vector, int largo) {
	bool haySecuencia = false;
	int contador = 0;
	int numeroAnterior = 0;
	int recorrido = 0;
	while (recorrido < largo) {
		if (vector[recorrido] == 1) {
			haySecuencia = true;
			numeroAnterior = vector[recorrido];
		}
		else if (vector[recorrido] == 2 && ((numeroAnterior == 1) || (numeroAnterior == 2 && haySecuencia == true))) {
			numeroAnterior = vector[recorrido];
		}
		else if (vector[recorrido] == 3 && numeroAnterior == 2 && haySecuencia == true) {
			contador++;
			haySecuencia = false;
		}
		else {
			haySecuencia = false;
		}

		recorrido++;
	}
	return contador;
}

int maximoNumero(unsigned int n) {
	int maximo = INT16_MIN;
	int numeroIngresado;
	while (n != 0) {
		cin >> numeroIngresado;
		if (numeroIngresado > maximo) {
			maximo = numeroIngresado;
		}
		n--;
	}
	return maximo;
}

//PRE: Recibe dos direcciones de memoria de enteros 'A' y 'B'.
//POS: Intercambia la direccion de memoria de 'A' por 'B' y vicerversa.
void intercambiar(int& a, int& b) {
	int aux = a;
	a = b;
	b = aux;
}

void ordenarVecInt(int* vec, int largoVec) {
	for (int i = 0; i < largoVec; i++)
	{
		for (int j = largoVec - 1; i < j; j--) {
			if (vec[j - 1] > vec[j]) {
				intercambiar(vec[j], vec[j - 1]);
			}
		}
	}
}

int largoDeString(char* str) {
	int largo = 0;

	while (str[largo] != '\0') {
		largo++;
	}
	return largo;
}

char* invertirCase(char* str)
{
	int recorrido = 0;
	int largo = largoDeString(str);

	char* nuevaCadena = new char[largo + 1];
	while (recorrido <= largo) {
		if (str[recorrido] >= 65 && str[recorrido] <= 90) {
			nuevaCadena[recorrido] = str[recorrido] + 32;
		}
		else if (str[recorrido] >= 97 && str[recorrido] <= 122) {
			nuevaCadena[recorrido] = str[recorrido] - 32;
		}
		else {
			nuevaCadena[recorrido] = str[recorrido];
		}
		recorrido++;
	}
	return nuevaCadena;
}

int islas(char** mapa, int col, int fil) {
	// IMPLEMENTAR SOLUCION
	return 0;
}

int ocurrencia(char* principal, char* subString) {
	int resultado = 0;
	int recorrido = 0;
	bool hayOcurrencia = false;
	int largoSubString = largoDeString(subString);
	int contador = 0;

	while (principal[recorrido] != '\0') {
		if (principal[recorrido] == subString[recorrido]) {
			contador++;
			hayOcurrencia = true;
			if (contador == largoSubString) {
				resultado = 1;
			}
		}
		else
		{
			hayOcurrencia = false;
		}
		recorrido++;
	}
	return resultado;
}

unsigned int ocurrenciasSubstring(char** vecStr, int largoVecStr, char* substr)
{
	// IMPLEMENTAR SOLUCION
	return NULL;
}


//PRE: recibe un puntero a la palabra A y otro puntero a la palabra B.
//POS: retorna el puntero cuya palabra sea menor en orden de la tabla ascii.
char* palabraMenor(char* palabraA, char* palabraB) {
	int largoPalabraA = largoDeString(palabraA);
	int largoPalabraB = largoDeString(palabraB);
	int recorrido = largoPalabraA;

	//Si el largo de la palabra B es menor, entonces voy a recorrer B porque es menor.
	if (largoPalabraA > largoPalabraB) {
		recorrido = largoPalabraB;
	}


	for (int i = 0; i < recorrido + 1; i++)
	{
		if (palabraA[i] < palabraB[i]) {
			return palabraA;
		}
		else if (palabraA[i] > palabraB[i]) {
			return palabraB;
		}
	}

	if (largoPalabraA > largoPalabraB) {
		return palabraA;
	}
	else {
		return palabraB;
	}

}

char** copiarVector(char** vector, int largo) {
	char** vectorDeCadenas = new char* [largo];

	for (int i = 0; i < largo; i++)
	{
		vectorDeCadenas[i] = vector[i];
	}
	return vectorDeCadenas;

}

bool esMayor (char* cadenaA, char* cadenaB) {
	if (cadenaA == NULL) {
		return false;
	}
	else if (cadenaB == NULL) {
		return true;
	}
	else
	{
		int recorrido = 0;
		while (cadenaA[recorrido] != '\0')
		{
			if (cadenaA[recorrido] < cadenaB[recorrido]) {
				return true;
			}
			else if (cadenaA[recorrido] > cadenaB[recorrido]) {
				return false;
			}
			else if(cadenaA[recorrido] == cadenaB[recorrido])
			{
				recorrido++;
			}
		}
		return false;
	}
}

void intercambiarValor(char* cadenaA, char* cadenaB) {
	char* auxiliar = cadenaA;
	cadenaA = cadenaB;
	cadenaB = auxiliar;
}

char** ordenarVecStrings(char** vecStr, int largoVecStr)
{
	if (vecStr != NULL) {
		char** cadenaOrdenada = new char* [largoVecStr];
		cadenaOrdenada = copiarVector(vecStr, largoVecStr);
		
		return cadenaOrdenada;
	}
	else {
		return NULL;
	}
}

//PRE: recibe un vector de enteros y su largo.
//POS: retorna un nuevo vector con los mismos elementos sin compartir memoria con el vector recibido por parametros.
int* copiarVectorEnteros(int* vec, int largoVec) {
	int* nuevoVector = new int[largoVec];
	for (int i = 0; i < largoVec; i++)
	{
		nuevoVector[i] = vec[i];
	}
	return nuevoVector;
}


int* intercalarVector(int* v1, int* v2, int l1, int l2) {

	if (l1 == 0 && l2 != 0) { //Si el v1 es vacio entonces no hay nada que intercalar, retorno un nuevo vector con los elementos de v2
		int* vectorIntercalado = new int[l2];
		vectorIntercalado = copiarVectorEnteros(v2, l2);
		return vectorIntercalado;
	}
	else if (l1 != 0 && l2 == 0) { //Si el v2 es vacio entonces no hay nada que intercalar, retorno un nuevo vector con los elementos de v1
		int* vectorIntercalado = new int[l1];
		vectorIntercalado = copiarVectorEnteros(v1, l1);
		return vectorIntercalado;
	}
	else if (l1 != 0 || l2 != 0) {
		int contadorV1 = 0;
		int contadorV2 = 0;
		int recorrido = 0;
		int largo = l1 + l2;
		int* vectorIntercalado = new int[largo];
		while (recorrido < largo) {

			if (contadorV1 < l1 && v1[contadorV1] <= v2[contadorV2]) { //Si la posicion actual de v1 es menor o igual a la de v2 entonces el vector intercalado se queda con el elemento de v1
				vectorIntercalado[recorrido] = v1[contadorV1];
				contadorV1++;
			}
			else if (contadorV2 < l2 && v2[contadorV2] < v1[contadorV1]) { //Si la posicion actual de v2 es menor a la de v1 entonces el vector intercalado se queda con el elemento de v2
				vectorIntercalado[recorrido] = v2[contadorV2];
				contadorV2++;
			}
			else if (contadorV1 < l1 && contadorV2 >= l2) { // Si ya recorri todo el vector v2, entonces solo sigo cargadando al vector intercalado los elementos restantes de v1 
				vectorIntercalado[recorrido] = v1[contadorV1];
				contadorV1++;
			}
			else if (contadorV1 >= l1 && contadorV2 < l2) { // Si ya recorri todo el vector v1, entonces solo sigo cargadando al vector intercalado los elementos restantes de v2 
				vectorIntercalado[recorrido] = v2[contadorV2];
				contadorV2++;
			}
			recorrido++;
		}
		return vectorIntercalado;
	}
	else { // Si no recibo vectores retorno NULL.
		return NULL;
	}
}

//PRE: Recibe un vector de enteros, su largo y un elemento.
//POS: Retorna true si el elemento existe en el vector o false en caso contrario.
bool existeElemento(int* vec, int largo, int elemento) {
	bool existe = false;
	for (int i = 0; i < largo; i++)
	{
		if (elemento == vec[i]) {
			existe = true;
		}
	}
	return existe;


}

bool subconjuntoVector(int* v1, int* v2, int l1, int l2)
{
	if (v1 == NULL) {
		return true;
	}
	else if (v2 == NULL) {
		return false;
	}
	else {
		bool esSubconjunto = true;
		for (int i = 0; i < l1; i++)
		{
			if (!existeElemento(v2, l2, v1[i])) {
				esSubconjunto = false;
			}
		}
		return esSubconjunto;
	}

}

char** splitStr(char* str, char separador, int& largoRet)
{
	// IMPLEMENTAR SOLUCION
	return NULL;
}

void ordenarVecIntMergeSort(int* vector, int largo)
{
	// IMPLEMENTAR SOLUCION
}
